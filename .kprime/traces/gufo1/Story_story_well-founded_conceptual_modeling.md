# Story GOAL story_well-founded_conceptual_modeling
+ goal 1617216226991

[Unified Foundational Ontology](https://en.wikipedia.org/wiki/Upper_ontology) (UFO), an axiomatic
formal theory based on theories from analytic metaphysics, philosophical logic, cognitive psychology,
and linguistics.

Referente: Giancarlo Guizzardi

[schema](https://excalidraw.com/#json=FBZcxIxFDpvC3QTeP7W0X,jnbHp6je9PrUMO8QGr3uVA)
